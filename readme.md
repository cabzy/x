# Enviormental variables

## Vault

##### VAULT_ADDR

the address of the vault server

#### VAULT_AGENT_ADDR

The vault agent address

#### VAULT_TOKEN

The token used to access vault

## Sonyflake

#### APP_SONYFLAKE_MACHINEID

The ID used to identify this machine

## Postgres

#### APP_DATABASE_ADDR

the address for postgres server

#### APP_DATABASE_DATABASE

The name of the postgres database

#### APP_DATABASE_AUTH_USERNAME

The username used to authenticate with the database

#### APP_DATABASE_AUTH_PASSWORD

The password used to authenticate with the database
