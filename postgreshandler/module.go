package postgreshandler

import (
	"github.com/go-pg/pg"
	"go.uber.org/fx"
)

//Module provides a module
var Module = fx.Provide(New)

//New creates a new instance of this module
func New(opts *pg.Options) *pg.DB {
	postgres := pg.Connect(opts)
	return postgres
}
