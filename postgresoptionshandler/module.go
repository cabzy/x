package postgresoptionshandler

import (
	"github.com/go-pg/pg"
	"github.com/spf13/viper"
	"go.uber.org/fx"
)

var Module = fx.Provide(NewPostgresOptions)

func NewPostgresOptions(v *viper.Viper) (*pg.Options, error) {
	if v.IsSet("database.url") {
		return pg.ParseURL(v.GetString("database.url"))
	}
	options := pg.Options{
		Addr:            v.GetString("database.addr"),
		Database:        "UserService",
		ApplicationName: "UserServiceServer",
	}
	if v.IsSet("database.database") {
		options.Database = v.GetString("database.database")
	}
	if v.IsSet("database.auth.username") {
		options.User = v.GetString("database.auth.username")
	}
	if v.IsSet("database.auth.password") {
		options.Password = v.GetString("database.auth.password")
	}

	return &options, nil
}
