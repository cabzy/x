package sonyflakehandler

import (
	"github.com/sony/sonyflake"
	"github.com/spf13/viper"
	"go.uber.org/fx"
)

//Module provides a module
var Module = fx.Provide(New)

func New(v *viper.Viper) (*sonyflake.Sonyflake, error) {
	return sonyflake.NewSonyflake(sonyflake.Settings{
		MachineID: func() (uint16, error) {
			return uint16(v.GetUint("sonyflake.machineid")), nil
		},
	}), nil
}
