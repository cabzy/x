package auth0handler

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"go.uber.org/fx"

	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
)

type Response struct {
	Message string `json:"message"`
}

type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}

type Auth0HandlerOptions struct {
	Domain   string
	Audience string
	Issuer   string
}

type Auth0Handler struct {
	viper  *viper.Viper
	logger *zerolog.Logger
	jwt    *jwtmiddleware.JWTMiddleware
}

var Module = fx.Invoke(New)

func New(viper *viper.Viper, logger *zerolog.Logger, options *Auth0HandlerOptions) *Auth0Handler {
	jwt := NewJWTMiddleware(options.Audience, options.Issuer, options.Domain)

	return &Auth0Handler{
		viper:  viper,
		logger: logger,
		jwt:    jwt,
	}
}

func (a *Auth0Handler) NewJWTHandler() gin.HandlerFunc {

	return func(c *gin.Context) {
		err := a.jwt.CheckJWT(c.Writer, c.Request)
		if err != nil {
			a.logger.Error().Err(err).Msg("A error occured when checking the jwt token")
			//c.Error(err)
			return
		}

		c.Next()
	}
}

func NewJWTMiddleware(domain string, audience string, issuer string) *jwtmiddleware.JWTMiddleware {
	return jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			// Verify 'aud' claim
			aud := audience
			checkAud := token.Claims.(jwt.MapClaims).VerifyAudience(aud, false)
			if !checkAud {
				return token, errors.New("Invalid audience.")
			}
			// Verify 'iss' claim
			iss := issuer
			checkIss := token.Claims.(jwt.MapClaims).VerifyIssuer(iss, false)
			if !checkIss {
				return token, errors.New("Invalid issuer.")
			}

			cert, err := getPemCert(token, domain)
			if err != nil {
				panic(err.Error())
			}

			result, _ := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))
			return result, nil
		},
		SigningMethod: jwt.SigningMethodRS256,
	})
}

func getPemCert(token *jwt.Token, domain string) (string, error) {
	cert := ""
	//https://johnstest.eu.auth0.com
	resp, err := http.Get(domain + "/.well-known/jwks.json")

	if err != nil {
		return cert, err
	}
	defer resp.Body.Close()

	var jwks = Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)

	if err != nil {
		return cert, err
	}

	for k, _ := range jwks.Keys {
		if token.Header["kid"] == jwks.Keys[k].Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + jwks.Keys[k].X5c[0] + "\n-----END CERTIFICATE-----"
		}
	}

	if cert == "" {
		err := errors.New("Unable to find appropriate key.")
		return cert, err
	}

	return cert, nil
}
