package viperhandler

import (
	"strings"

	"github.com/spf13/viper"
	"go.uber.org/fx"
)

//Module provides a module
var Module = fx.Provide(New)

//New Create new instance of viper
func New() (*viper.Viper, error) {
	v := viper.New()

	v.AllowEmptyEnv(true)
	v.SetEnvPrefix("app")
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	v.AutomaticEnv()

	v.SetConfigName("config")
	v.SetConfigType("json")
	err := v.ReadInConfig()
	if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
		// Config file not found; ignore error if desired
		return nil, err
	}
	//clear the error as it was only file not found
	err = nil
	if v.IsSet("config.remote.type") {
		if v.IsSet("config.remote.gpg") {
			v.AddSecureRemoteProvider(v.GetString("config.remote.type"), v.GetString("config.remote.addr"), v.GetString("config.remote.path"), v.GetString(viper.GetString("config.remote.gpg")))
		} else {
			viper.AddRemoteProvider(v.GetString("config.remote.type"), v.GetString("config.remote.addr"), v.GetString("config.remote.path"))
		}
		v.SetConfigType(v.GetString("config.remote.ext"))
		err = v.ReadRemoteConfig()
	}

	return v, err
}
