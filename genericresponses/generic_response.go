package genericresponses

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/cabzy/x.git/requestidhandler"
)

//SuccessMessage represents a sucessful request
const SuccessMessage = "The request was sucessfully completed"

//CreatedMessage represents a resource being created, the message will normally contain the resources identifier
const CreatedMessage = "The request sucessfully created a resource, the payload will contain the ID"

//BadRequest represents a request being malformed
const BadRequestMessage = "The request was malformed you may retry this request with the correct format"

//NotFoundMessage represents a resource that could not be found
const NotFoundMessage = "The requested resource could not be found"

//InternalErrorMessage represents a error that has happened internally
const InternalErrorMessage = "A internal error occured, if this continues please contact support"

//GenericResponse is used to respond to a request
type GenericResponse struct {
	Code      int         `json:"code"`
	Message   string      `json:"message,omitempty"`
	Payload   interface{} `json:"payload,omitempty"`
	RequestID uint64      `json:"request_id,string,omitempty"`
}

func GinJSON(c *gin.Context, resp GenericResponse) {
	resp.RequestID = requestidhandler.GetRequestID(c)
	if resp.Code == 0 {
		resp.Code = 200
	}
	if resp.Code == http.StatusOK {
		if resp.Message == "" {
			resp.Message = SuccessMessage
		}
	}
	if resp.Code == http.StatusCreated {
		if resp.Message == "" {
			resp.Message = CreatedMessage
		}
	}
	if resp.Code == http.StatusNotFound {
		if resp.Message == "" {
			resp.Message = NotFoundMessage
		}
	}
	if resp.Code == http.StatusInternalServerError {
		if resp.Message == "" {
			resp.Message = InternalErrorMessage
		}
	}
	if resp.Code == http.StatusBadRequest {
		if resp.Message == "" {
			resp.Message = BadRequestMessage
		}
	}
	c.JSON(resp.Code, resp)
}

func OK(c *gin.Context, payload interface{}) {
	GinJSON(c, GenericResponse{
		Code:    http.StatusOK,
		Payload: payload,
	})
}

func Created(c *gin.Context, identifier interface{}) {
	GinJSON(c, GenericResponse{
		Code:    http.StatusCreated,
		Payload: identifier,
	})
}

func NotFound(c *gin.Context) {
	GinJSON(c, GenericResponse{
		Code: http.StatusNotFound,
	})
}

func InternalError(c *gin.Context) {
	GinJSON(c, GenericResponse{
		Code: http.StatusInternalServerError,
	})
}

func BadRequest(c *gin.Context) {
	GinJSON(c, GenericResponse{
		Code: http.StatusBadRequest,
	})
}
