package requestidhandler

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/sony/sonyflake"
	"go.uber.org/fx"
)

const RequestIDTag = "requestID"

var Module = fx.Provide(New)

type RequestIDHandler struct {
	sonyflake *sonyflake.Sonyflake
}

func New(sonyflake *sonyflake.Sonyflake) *RequestIDHandler {
	return &RequestIDHandler{
		sonyflake: sonyflake,
	}
}

func (s *RequestIDHandler) Handler() gin.HandlerFunc {
	return func(c *gin.Context) {
		reqID := c.Query(RequestIDTag)
		if reqID != "" {
			c.Set(RequestIDTag, reqID)
		}

		id, err := s.sonyflake.NextID()
		if err == nil {
			c.Set(RequestIDTag, id)
		} else {
			c.Error(err)
		}
	}
}

func GetRequestID(c *gin.Context) uint64 {
	reqID, exists := c.Get(RequestIDTag)
	if !exists {
		return 0
	}
	id, ok := reqID.(uint64)
	if !ok {
		return 0
	}
	return id
}

func GetRequestLogger(c *gin.Context, logger *zerolog.Logger) *zerolog.Logger {
	loggera := logger.With().Uint64(RequestIDTag, GetRequestID(c)).Logger()

	return &loggera
}
