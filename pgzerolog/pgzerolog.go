package pgzerolog

import (
	"encoding/json"

	"go.uber.org/fx"

	"github.com/go-pg/pg"
	"github.com/rs/zerolog"
)

var Module = fx.Invoke(New)

func New(pg *pg.DB, logs *zerolog.Logger) {
	pg.AddQueryHook(PGZeroLog{
		Log: logs,
	})
}

type PGZeroLog struct {
	Log *zerolog.Logger
}

func (p PGZeroLog) BeforeQuery(q *pg.QueryEvent) {
}

func (p PGZeroLog) AfterQuery(q *pg.QueryEvent) {
	qry, err := q.UnformattedQuery()

	data, jerr := json.Marshal(q.Data)

	var event *zerolog.Event
	if q.Error != nil {
		event = p.Log.Error().Err(q.Error)
	} else {
		event = p.Log.Debug()
	}

	if jerr == nil {
		event.Str("data", string(data))
	}

	if q.Result != nil {
		event.Int("rows_returned", q.Result.RowsReturned()).Int("rows_affected", q.Result.RowsAffected())
	}

	if q.Error != nil {
		p.Log.Error().Str("query", qry).Err(err).Msg("Query errored")
	} else {
		p.Log.Debug().Str("query", qry).Err(q.Error).Int("rows_returned", q.Result.RowsReturned()).Int("rows_affected", q.Result.RowsAffected()).Msg("Sucessful Query")
	}
}
