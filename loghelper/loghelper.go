package loghelper

import (
	"runtime"

	"github.com/gin-gonic/gin"
	"gitlab.com/cabzy/x.git/requestidhandler"

	"github.com/rs/zerolog"
)

type LoggerEventHelper struct {
	Event *zerolog.Event
}

func Debug(logger *zerolog.Logger) *LoggerEventHelper {
	return &LoggerEventHelper{
		Event: logger.Debug(),
	}
}

func Info(logger *zerolog.Logger) *LoggerEventHelper {
	return &LoggerEventHelper{
		Event: logger.Info(),
	}
}

func Warn(logger *zerolog.Logger) *LoggerEventHelper {
	return &LoggerEventHelper{
		Event: logger.Warn(),
	}
}

func Error(logger *zerolog.Logger) *LoggerEventHelper {
	return &LoggerEventHelper{
		Event: logger.Error(),
	}
}

func Fatal(logger *zerolog.Logger) *LoggerEventHelper {
	return &LoggerEventHelper{
		Event: logger.Fatal(),
	}
}

func Panic(logger *zerolog.Logger) *LoggerEventHelper {
	return &LoggerEventHelper{
		Event: logger.Panic(),
	}
}

func (e *LoggerEventHelper) AddCallerInformation() *LoggerEventHelper {
	pc, fn, line, ok := runtime.Caller(1)
	if ok {
		e.Event = e.Event.Str("file_name", fn).Str("func_name", runtime.FuncForPC(pc).Name()).Int("line_num", line)
	}
	return e
}

func (e *LoggerEventHelper) AddRequestID(ctx *gin.Context) *LoggerEventHelper {
	e.Event = e.Event.Uint64(requestidhandler.RequestIDTag, requestidhandler.GetRequestID(ctx))
	return e
}

func (e *LoggerEventHelper) GetEvent() *zerolog.Event {
	return e.Event
}

func (e *LoggerEventHelper) Msg(msg string) {
	e.Event.Msg(msg)
}

func (e *LoggerEventHelper) Send() {
	e.Event.Send()
}
