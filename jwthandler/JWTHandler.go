package jwthandler

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"gitlab.com/cabzy/x.git/requestidhandler"
	"go.uber.org/fx"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
)

var (
	//ErrNoAuthHeader means that a auth header was not found in context
	ErrNoAuthHeader = errors.New("jwthandler: Auth header not found")
	//ErrAuthNotBearer auth header was not of type bearer
	ErrAuthNotBearer = errors.New("jwthandler: Auth header was not of type bearer")
	//CtxUserToken is used in gin context to get the users token
	CtxUserToken = "user_token"
	//CtxUserClaims is used in gin context to get the users claims(like sub etc)
	CtxUserClaims = "user_claims"
)

//Module provides a JWTHandler to the fx instance
var Module = fx.Provide(New)

//JWTHandler handles jwt/jwk interpreting
type JWTHandler struct {
	Subject  string
	Audience []string
	Issuer   string
	//JWK            *jose.JSONWebKey
	JWKs           []jose.JSONWebKey
	ExpectedClaims *jwt.Expected
	logger         *zerolog.Logger
}

func GetTokenContext(c *gin.Context) *jwt.JSONWebToken {
	val, exists := c.Get(CtxUserToken)
	if !exists {
		return nil
	}
	claim, ok := val.(*jwt.JSONWebToken)
	if !ok {
		return nil
	}
	return claim
}

func GetClaimsContext(c *gin.Context) *jwt.Claims {
	val, exists := c.Get(CtxUserClaims)
	if !exists {
		return nil
	}
	claim, ok := val.(*jwt.Claims)
	if !ok {
		return nil
	}
	return claim
}

//New creates a new JWTHandler
func New(viper *viper.Viper, logger *zerolog.Logger) (*JWTHandler, error) {
	resp, err := http.Get(viper.GetString("jwt.jwk.url"))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return nil, err
	}

	jwks, err := getJWKSFromJSON(body, logger)
	if err != nil {
		return nil, err
	}

	jwtExpected := jwt.Expected{}
	jwtExpected.Audience = viper.GetStringSlice("jwt.audience")
	if viper.GetString("jwt.subject") != "" {
		jwtExpected.Subject = viper.GetString("jwt.subject")
	}
	if viper.GetString("jwt.issuer") != "" {
		jwtExpected.Issuer = viper.GetString("jwt.issuer")
	}

	return &JWTHandler{
		Subject:  viper.GetString("jwt.subject"),
		Audience: viper.GetStringSlice("jwt.audience"),
		Issuer:   viper.GetString("jwt.issuer"),
		//JWK:            jwk,
		JWKs:           jwks,
		ExpectedClaims: &jwtExpected,
		logger:         logger,
	}, nil
}

func getJWKSFromJSON(jsonJWKs []byte, logger *zerolog.Logger) ([]jose.JSONWebKey, error) {
	var unmarshaled map[string]interface{}
	var err error
	json.Unmarshal(jsonJWKs, &unmarshaled)
	keys := unmarshaled["keys"].([]interface{})

	jwks := make([]jose.JSONWebKey, 0)
	for _, value := range keys {
		byteJSON, err := json.Marshal(value)
		if err == nil {
			jwk, err := loadJWK(byteJSON, true)
			if err == nil {
				jwks = append(jwks, *jwk)
			} else {
				logger.Debug().Err(err).Msg("Failed to load JWK")
			}
			//append(jwks, )
		} else {
			logger.Debug().Err(err).Msg("Failed to marshal json")
		}
	}
	if err != nil {
		return nil, err
	}
	return jwks, err
}

//Handler will check and validate jwt tokens, it then sets CtxUserToken as a *jwt.JSONWebToken and CtxUserClaims as a *jwt.Claims{}
func (j *JWTHandler) Handler() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenStr, err := getBearerAuth(c)
		if err != nil {
			j.logger.Debug().Uint64(requestidhandler.RequestIDTag, requestidhandler.GetRequestID(c)).Err(err).Msg("Could not get Bearer auth from token")
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		// json, _ := j.JWK.MarshalJSON()

		// j.logger.Info().Uint64(requestidhandler.RequestIDTag, requestidhandler.GetRequestID(c)).Str("jwk", string(json)).Str("Token", tokenStr).Msg("Token value debug")

		token, err := jwt.ParseSigned(tokenStr)
		if err != nil {
			j.logger.Debug().Uint64(requestidhandler.RequestIDTag, requestidhandler.GetRequestID(c)).Err(err).Msg("Could not parse a signed token")
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		//j.logger.Info().Msg("Token is done")

		// out := jwt.Claims{}
		// err = token.Claims(j.JWK, &out)
		// if err != nil {
		// 	j.logger.Debug().Uint64(requestidhandler.RequestIDTag, requestidhandler.GetRequestID(c)).Err(err).Msg("Could not parse claims")
		// 	c.AbortWithStatus(http.StatusUnauthorized)
		// 	return
		// }

		claims := j.validateJWKs(token)

		if claims == nil {
			panic(errors.New("Claims was null"))
		}

		err = claims.Validate(j.ExpectedClaims.WithTime(time.Now()))
		if err != nil {
			j.logger.Debug().Uint64(requestidhandler.RequestIDTag, requestidhandler.GetRequestID(c)).Err(err).Msg("Could not validate claims")
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		c.Set(CtxUserToken, token)
		c.Set(CtxUserClaims, claims)

		c.Next()
	}
}

func (j *JWTHandler) validateJWKs(token *jwt.JSONWebToken) *jwt.Claims {
	fmt.Println(j.JWKs)
	for _, value := range j.JWKs {
		out := jwt.Claims{}
		err := token.Claims(value, &out)
		if err == nil {
			return &out
		}
	}

	return nil
}

func getBearerAuth(c *gin.Context) (string, error) {
	auth := c.GetHeader("Authorization")
	if auth == "" {
		return "", ErrNoAuthHeader
	}

	if !strings.Contains(auth, "Bearer") {
		return "", ErrAuthNotBearer
	}

	//strings.Replace(auth, "Bearer ", str, 1)
	return strings.Replace(auth, "Bearer ", "", 1), nil
}

func loadJWK(json []byte, pub bool) (*jose.JSONWebKey, error) {
	var jwk jose.JSONWebKey
	err := jwk.UnmarshalJSON(json)
	if err != nil {
		return nil, err
	}
	if !jwk.Valid() {
		return nil, errors.New("invalid JWK key")
	}
	if jwk.IsPublic() != pub {
		return nil, errors.New("priv/pub JWK key mismatch")
	}
	return &jwk, nil
}
