package loghandler

import (
	"os"

	"github.com/rs/zerolog"
	"go.uber.org/fx"
)

//Module provides a module
var Module = fx.Provide(New)

//New Create new logger
func New() *zerolog.Logger {
	log := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr}).With().Timestamp().Logger()
	return &log
}
