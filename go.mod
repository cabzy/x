module gitlab.com/cabzy/x.git

go 1.13

require (
	github.com/auth0/go-jwt-middleware v0.0.0-20190805220309-36081240882b
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gin-gonic/gin v1.6.2
	github.com/go-openapi/strfmt v0.19.5
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/hashicorp/vault/api v1.0.4
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/mitchellh/mapstructure v1.2.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/ory/hydra-client-go v1.4.5
	github.com/pelletier/go-toml v1.7.0 // indirect
	github.com/rs/zerolog v1.18.0
	github.com/sony/sonyflake v1.0.0
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.3
	go.uber.org/fx v1.12.0
	go.uber.org/multierr v1.5.0 // indirect
	gocloud.dev v0.19.0
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/sys v0.0.0-20200413165638-669c56c373c4 // indirect
	golang.org/x/tools v0.0.0-20200416040550-9c19d0db369a // indirect
	gopkg.in/auth0.v4 v4.3.4
	gopkg.in/ini.v1 v1.55.0 // indirect
	gopkg.in/square/go-jose.v2 v2.3.1
	honnef.co/go/tools v0.0.1-2020.1.3 // indirect
	mellium.im/sasl v0.2.1 // indirect
)
