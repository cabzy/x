package hydrahandler

import (
	"net/url"

	"github.com/go-openapi/strfmt"

	"github.com/ory/hydra-client-go/client"
	"github.com/spf13/viper"
	"go.uber.org/fx"
)

var Module = fx.Provide(New)

func New(v *viper.Viper) *client.OryHydra {
	hydraURL, _ := url.Parse(v.GetString("hydra.url"))
	transportConfig := client.TransportConfig{}
	transportConfig.WithHost(hydraURL.Host)
	transportConfig.WithSchemes([]string{hydraURL.Scheme})
	transportConfig.WithBasePath(hydraURL.Path)
	hydra := client.NewHTTPClientWithConfig(strfmt.Default, &transportConfig)

	return hydra
	//return client.NewHTTPClientWithConfig(nil, &transportConfig)
	//return client.NewHTTPClientWithConfig(nil, &client.TransportConfig{Schemes: []string{hydraURL.Scheme}, Host: hydraURL.Host, BasePath: hydraURL.Path})
}
