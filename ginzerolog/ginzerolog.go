package ginzerolog

import (
	"net"
	"time"

	"github.com/rs/zerolog"

	"github.com/gin-gonic/gin"
)

// var Module = fx.Provide()

// type GinZeroLog struct {
// }

// func New() {

// }

// func (l *GinZeroLog) Handler() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		currentTime := time.Now()
// 		path := c.Request.URL.Path
// 		raw := c.Request.URL.RawQuery
// 		c.Next()
// 		if raw != "" {
// 			path = path + "?" + raw
// 		}
// 		msg := c.Errors.String()
// 		if msg == "" {
// 			msg = "Request"
// 		}

// 		var event *zerolog.Event
// 		if c.Writer.Status() >= 400 && c.Writer.Status() < 500 {
// 			event = log.Warn()
// 		} else if c.Writer.Status() >= 500 {
// 			event = log.Error()
// 		} else {
// 			event = log.Debug()
// 		}
// 		reqID, reqIDExists := c.Get("request_id")
// 		if reqIDExists {
// 			event.Str("requestid", reqID.(string))
// 		}

// 		event.Str("method", c.Request.Method).Str("useragent", c.Request.UserAgent()).Str("path", path).Dur("latency", time.Since(currentTime)).Int("status", c.Writer.Status()).IPAddr("remote_addr", net.ParseIP(c.Request.RemoteAddr)).Msg(msg)
// 	}
// }

//Logger /
func Logger(serviceName string, log *zerolog.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		currentTime := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery
		c.Next()
		if raw != "" {
			path = path + "?" + raw
		}
		msg := c.Errors.String()
		if msg == "" {
			msg = "Request"
		}

		var event *zerolog.Event
		if c.Writer.Status() >= 400 && c.Writer.Status() < 500 {
			event = log.Warn()
		} else if c.Writer.Status() >= 500 {
			event = log.Error()
		} else {
			event = log.Debug()
		}

		reqID, reqIDExists := c.Get("request_id")
		if reqIDExists {
			event.Str("requestid", reqID.(string))
		}

		event.Str("method", c.Request.Method).Str("useragent", c.Request.UserAgent()).Str("path", path).Dur("latency", time.Since(currentTime)).Int("status", c.Writer.Status()).IPAddr("remote_addr", net.ParseIP(c.Request.RemoteAddr)).Msg(msg)
	}
}
