package vaulthandler

import (
	"github.com/hashicorp/vault/api"
	"github.com/spf13/viper"
	"go.uber.org/fx"
)

//Module provides a module
var Module = fx.Provide(New)

func New(v *viper.Viper) (*api.Client, error) {
	return api.NewClient(nil)
}
