package auth0managment

import (
	"fmt"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"go.uber.org/fx"
	"gopkg.in/auth0.v4/management"
)

var Module = fx.Invoke(New)

func New(viper *viper.Viper, logger *zerolog.Logger) *management.Management {
	fmt.Println(viper.GetString("auth0.domain"))
	fmt.Println(viper.GetString("auth0.client.id"))
	fmt.Println(viper.GetString("auth0.client.secret"))
	managment, err := management.New(viper.GetString("auth0.domain"), viper.GetString("auth0.client.id"), viper.GetString("auth0.client.secret"), func(m *management.Management) {})

	if err != nil {
		logger.Error().Err(err).Msg("Management errored on creation")
	}

	if managment != nil {
		logger.Error().Msg("Management was null")
	}

	return managment

	//return &Auth0Managment{Managment: managment}
}
